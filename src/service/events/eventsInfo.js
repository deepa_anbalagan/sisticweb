import {AUTH_TOKEN, BASE_URL} from '../../constants/common-info';


const config = {
    headers: {
        Authorization: `Bearer ${AUTH_TOKEN}`,
        'Content-type': 'application/json; charset=UTF-8',
    }
};

export async function getInfo(){

    return await fetch(BASE_URL + "/icc/ccoffee0217", config).then((res) => {
        return res.json()
    }).catch(error => {
        console.log("SOMETHING WENT WRONG")
    });
}
