import React, { Component } from 'react'
import Home from './components/events/Home'
import { Provider } from 'react-redux'
import {showInfoReducer} from './reducers/showInfoReducer'
import { createStore} from 'redux';

let store = createStore(showInfoReducer);
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
  }

  render(){
    return(
              <Provider store={store}>
                <Home {...this.props} />
              </Provider>
    )
  }
}

export default App;
