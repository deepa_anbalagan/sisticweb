import {SHOW_INFO} from '../actions/actionsTypes'

const initialState = {
    showInfo: [],
    error: null
}

export function showInfoReducer(state = initialState, action) {
    switch(action.type) {
        case "SHOW_INFO":
            return {
                ...state,
                showInfo: action.showInfo
            };
        default:
            return state;
    }
}


