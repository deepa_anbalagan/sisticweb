// TODO: Deepa should move this to state so that changing here will be directly applied
export const OUR_COMPANY = 'Our Company';
export const LINKS = 'Helpful Links';
export const AD = 'SISTIC on Mobile';
export const CONNECTION = 'Stay Connected';
export const ABOUT_US = 'About us';
export const SELL_WITH_US = 'Sell with Us';
export const TIC_TECH = 'Ticketing Technology';
export const PARTNER = 'Partner with us';
export const CAREERS = 'Careers';
export const LINKS_WHERE_TO_BUY = 'Where to buy Tickets';
export const LINKS_LOCATE_AGENT = 'Locate an Agent';
export const LINKS_LOCATE_VENUE = 'Locate an Venue';
export const LINK_BLOG = 'Blog';
export const LINK_MEDIA = 'Media';
