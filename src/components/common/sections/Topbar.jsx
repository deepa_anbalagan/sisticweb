import React, {Component} from 'react'


export default class Topbar extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
        <div className="row">
            <div className="col-8">
                <a href="#" className="logo-sec">
                    <img src={process.env.PUBLIC_URL + '/assets/images/sistic_logo.svg'} alt="sistic logo" className="img-fluid" />
                </a>
                <div className="search-sec">
                    <form className="search-main" id="search-form">
                        <input type="text" name="search" id="search" placeholder="Search experiences..." className="form-control" />
                        <img src={process.env.PUBLIC_URL + "/assets/images/search_icon.svg"} alt="search" className="img-fluid search_icon" />
                    </form>
                </div>
            </div>
            <div className="col-4 text-right ticket-main">
                <img src={process.env.PUBLIC_URL + "/assets/images/user_icon.svg"} alt="user" className="img-fluid" />
                <img src={process.env.PUBLIC_URL + "/assets/images/cart_icon.svg"} alt="cart" className="img-fluid" />
                <div className="ticket-with-us">
                    <a href="#">Ticket with us</a>
                </div>
            </div>
        </div>
      )
    }
}


