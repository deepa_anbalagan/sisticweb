import React, {Component} from 'react'


export default class MobileHeader extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="row">
                <div className="col-2">
                    <nav className="navbar navbar-dark">
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#custom-mobile-nav">
                        <span className="navbar-toggler-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                    <div className="collapse navbar-collapse" id="custom-mobile-nav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Events</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Attractions</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Promotions</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Explore</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Concert</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Musical</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Theatre</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Comedy</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="#" id="navbardrop"
                                data-toggle="dropdown">
                                More <span className="down_arrow"><img src={process.env.PUBLIC_URL + "/assets/images/chevron-down.svg"} /></span>
                            </a>
                            <div className="dropdown-menu">
                                <a className="dropdown-item" href="#">Link 1</a>
                                <a className="dropdown-item" href="#">Link 2</a>
                                <a className="dropdown-item" href="#">Link 3</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div className="col-3 pl-0">
            <a href="#" className="logo-sec">
                <img src={process.env.PUBLIC_URL + "/assets/images/sistic_logo.svg"} alt="sistic logo" className="img-fluid" />
            </a>
        </div>
        <div className="col-7 text-right ticket-main">
            <div className="search-sec">
                <form className="search-main" id="search-form">
                    <input type="text" name="search" id="search" placeholder="Search experiences..."
                    className="form-control" />
                    <img src={process.env.PUBLIC_URL + "/assets/images/search_icon.svg"} alt="search" className="img-fluid search_icon" />
                </form>
            </div>
            <a href="javascript:;"><img src={process.env.PUBLIC_URL + "/assets/images/cart_icon.svg"} alt="cart" className="img-fluid"/></a>
        </div>
    </div>
      )
    }
}


