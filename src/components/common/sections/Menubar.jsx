import React, {Component} from 'react'


export default class Menubar extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="row mt-3">
                <div className="col-6">
                    <nav className="navbar navbar-expand-sm">
                        <div className="collapse navbar-collapse" id="custom-left-nav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Events</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Attractions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Promotions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Explore</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="col-6 right-nav">
                    <nav className="navbar navbar-expand-sm">
                        <div className="collapse navbar-collapse" id="custom-right-nav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Concert</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Musical</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Theatre</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Comedy</a>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbardrop"
                                    data-toggle="dropdown">
                                    More <span className="down_arrow"><img src={process.env.PUBLIC_URL + "/assets/images/chevron-down.svg"} /></span>
                                </a>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" href="#">Link 1</a>
                                    <a className="dropdown-item" href="#">Link 2</a>
                                    <a className="dropdown-item" href="#">Link 3</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>
      )
    }
}


