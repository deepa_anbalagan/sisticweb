import React, {Component} from 'react'


export class Header extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <header>
                <div className="header" id="desktop-header">
                    <div className="container-fluid pad-x-30">
                        <div className="row">
                            <div className="col-8">
                                <a href="#" className="logo-sec">
                                    <img src="assets/images/sistic_logo.svg" alt="sistic logo" className="img-fluid" />
                                </a>
                                <div className="search-sec">
                                    <form className="search-main" id="search-form">
                                        <input type="text" name="search" id="search" placeholder="Search experiences..." className="form-control" />
                                        <img src="assets/images/search_icon.svg" alt="search" className="img-fluid search_icon" />
                                    </form>
                                </div>
                            </div>
                            <div className="col-4 text-right ticket-main">
                                <img src="assets/images/user_icon.svg" alt="user" className="img-fluid" />
                                <img src="assets/images/cart_icon.svg" alt="cart" className="img-fluid" />
                                <div className="ticket-with-us">
                                    <a href="#">Ticket with us</a>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col-6">
                                <nav className="navbar navbar-expand-sm">
                                    <div className="collapse navbar-collapse" id="custom-left-nav">
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Events</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Attractions</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Promotions</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Explore</a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <div className="col-6 right-nav">
                                <nav className="navbar navbar-expand-sm">
                                    <div className="collapse navbar-collapse" id="custom-right-nav">
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Concert</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Musical</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Theatre</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Comedy</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#" id="navbardrop"
                                                data-toggle="dropdown">
                                                More <span className="down_arrow"><img src="assets/images/chevron-down.svg" /></span>
                                            </a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item" href="#">Link 1</a>
                                                <a className="dropdown-item" href="#">Link 2</a>
                                                <a className="dropdown-item" href="#">Link 3</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
 
                        </div>
                    </div>

                </div>
            </div>

            <div className="header" id="mobile-header">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-2">
                            <nav className="navbar navbar-dark">
                                <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#custom-mobile-nav">
                                <span className="navbar-toggler-icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
                            </button>
                            <div className="collapse navbar-collapse" id="custom-mobile-nav">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Events</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Attractions</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Promotions</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Explore</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Concert</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Musical</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Theatre</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#">Comedy</a>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbardrop"
                                        data-toggle="dropdown">
                                        More <span className="down_arrow"><img src="assets/images/chevron-down.svg" /></span>
                                    </a>
                                    <div className="dropdown-menu">
                                        <a className="dropdown-item" href="#">Link 1</a>
                                        <a className="dropdown-item" href="#">Link 2</a>
                                        <a className="dropdown-item" href="#">Link 3</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div className="col-3 pl-0">
                    <a href="#" className="logo-sec">
                        <img src="assets/images/sistic_logo.svg" alt="sistic logo" className="img-fluid" />
                    </a>
                </div>
                <div className="col-7 text-right ticket-main">
                    <div className="search-sec">
                        <form className="search-main" id="search-form">
                            <input type="text" name="search" id="search" placeholder="Search experiences..."
                            className="form-control" />
                            <img src="assets/images/search_icon.svg" alt="search" className="img-fluid search_icon" />
                        </form>
                    </div>
                    <a href="javascript:;"><img src="assets/images/cart_icon.svg" alt="cart" className="img-fluid"/></a>
                </div>
            </div>
         </div>
        </div>
        </header>
      )
    }
}


