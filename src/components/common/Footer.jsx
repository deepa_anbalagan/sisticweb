import React, {Component} from 'react'
import { OUR_COMPANY, ABOUT_US, LINKS_LOCATE_AGENT, SELL_WITH_US, AD, CAREERS, LINK_MEDIA, CONNECTION, LINK_BLOG, LINKS, LINKS_LOCATE_VENUE, LINKS_WHERE_TO_BUY, PARTNER, TIC_TECH} from "../../constants/foot-utils";


export class Footer extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <footer>
                <div className="footer-top mt-5 p-4 mob-px-0">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-2 col-md-4 col-12">
                                <h4>Our Company</h4>
                                <div className="company-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">About us</a>
                                        </li>
                                        <li>
                                            <a href="#">Sell with Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Ticketing Technology</a>
                                        </li>
                                        <li>
                                            <a href="#">Partner with Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Careers</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-4 col-md-8 col-12">
                                <h4>Helpful Links</h4>
                                <div className="help-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">Where to Buy Tickets</a>
                                        </li>
                                        <li>
                                            <a href="#">Locate an Agent</a>
                                        </li>
                                        <li>
                                            <a href="#">Locate an Venue</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Media</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="account-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">My Account</a>
                                        </li>
                                        <li>
                                            <a href="#">Gift Vouchers</a>
                                        </li>
                                        <li>
                                            <a href="#">FAQ</a>
                                        </li>
                                        <li>
                                            <a href="#">Cancellations/Refunds</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-12">
                                <h4>SISTIC on Mobile</h4>
                                <div className="mobile-app">
                                    <a href="#" className="clearfix">
                                        <img src="assets/images/apple_bg.png" alt="apple" className="img-fluid appl-img" />
                                    </a>
                                    <a href="#">
                                    <img src="assets/images/play_bg.png" alt="play store" className="img-fluid play-img" />
                                </a>
                            </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-12">
                                <h4>Stay Connected</h4>
                                <div className="mail-inner">
                                    <form className="mail-sub" id="mail-form">
                                        <input type="email" name="user_email" id="user_email" placeholder="Enter your email" className="form-control" />
                                        <img src="assets/images/button_arrow.png" alt="Button arrow" className="img-fluid button-arrow" />
                                    </form>
                                </div>
                                <div className="follow-us">
                                <h4>Follow us on</h4>
                                <ul className="list-inline">
                                    <li>
                                        <a href="#">
                                            <img src="assets/images/footer_fb.png" alt="Facebook" className="img-fluid" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/images/footer_insta.png" alt="Instagram" className="img-fluid" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                    </div>

                </div>
            </div>
        </div>
        <div className="footer-bottom" >
            <div className="container-fluid" >
                <div className="row align-items-center hide-mob" >
                    <div className="col-lg-6 col-md-4 stix-sec">
                        <a href="#">
                            <img src="assets/images/footer_stix.gif" alt="Stix" className="img-fluid" />
                        </a>
                        <a href="#">
                            <img src="assets/images/google_trust.JPG" alt="Google trust" className="img-fluid" />
                        </a>
                        <p className="copyright">Copyright 1998 - 2019. &copy; SISTIC.com Pte Ltd</p>
                    </div>
                    <div className="col-lg-3 col-md-4 privacy-sec">
                        <a href="#">
                            Privacy Policy
                        </a>
                        <a href="#">
                            Terms & Conditions
                        </a>
                    </div>
                    <div className="col-lg-3 col-md-4 sistic-phone">
                        <p>
                            SISTIC Hotline: <a href="tel:+65 63485555">+65 63485555</a>
                        </p>
                    </div>
                </div>
                <div className="row align-items-center show-mob" >
                    <div className="stix-sec mb-3">
                        <a href="#">
                            <img src="assets/images/footer_stix.gif" alt="Stix" className="img-fluid" />
                        </a>
                        <a href="#">
                            <img src="assets/images/google_trust.JPG" alt="Google trust" className="img-fluid" />
                        </a>
                    </div>
                    <div className="privacy-sec mb-3">
                        <p className="copyright">Copyright 1998 - 2019. &copy; SISTIC.com Pte Ltd</p><br/>
                        <a href="#">
                            Privacy Policy
                        </a>
                        <a href="#">
                            Terms & Conditions
                        </a><br/>
                        <p>
                            SISTIC Hotline: <a href="tel:+65 63485555">+65 63485555</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </footer>
        )
    }
}
