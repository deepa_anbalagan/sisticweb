import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {Footer} from '../common/Footer'
import {Header} from '../common/Header'
import {Breadcrumbs} from './Breadcrumbs'
import {EventsQuantity} from './EventsQuantity'


class Home extends Component {

    constructor(props) {
        super(props);

    }

    async componentDidMount() {
        const response = await API.getInfo();
        this.props.fetchData(response);
    }

    render() {
        return (

            <Fragment>
                <Header/>

                <main>

                    <Breadcrumbs />

                    <EventsQuantity />

                </main>

                <Footer/>
            </Fragment>
        );
    }
}

