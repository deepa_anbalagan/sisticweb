import React, {Component} from 'react'

export class Description extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <section className="sec-2 mt-5" >
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-8 col-md-7 col-12 mob-px-0">
                            <div className="accordion pl-3 mob-px-0" id="detail-accordion">
                                <div className="card line">
                                    <div className="card-header pos-1" id="headingOne">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span>Synopsis</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>

                                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne"
                                         data-parent="#detail-accordion">
                                        <div className="card-body">
                                            <div className="col px-0 text-right">
                                                <div className="btn-group lang">
                                                    <button className="btn active">EN</button>
                                                    <button className="btn">CN</button>
                                                </div>
                                            </div>
                                            <p className="p1"> Playing final performances with limited VIP seats remaining, book now before tickets sell out! </p>
                                            <p className="p1">From the producer of The Lion King, the beloved story of Aladdin comes to life in this spectacular new musical. Breathtaking sets, mind-blowing special effects, over 300 lavish costumes and a fabulous cast bring the magic of Disney’s Aladdin to life on stage.</p>
                                            <p className="p1">
                                                Featuring all the songs from the classic Academy award winning film, including “Friend like Me”, “A Whole New World” and “Arabian Nights”, prepare to experience the unmissable theatrical magic that is Aladdin.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="card line">
                                    <div className="card-header pos-1" id="headingTwo">
                                        <button className="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span>Details</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>

                                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                         data-parent="#detail-accordion">
                                        <div className="card-body">
                                            <p className="p1"> Playing final performances with limited VIP seats remaining, book
                                                now before tickets sell out!
                                                From the producer of The Lion King, the beloved story of Aladdin comes to
                                                life in this spectacular
                                                new musical. Breathtaking sets, mind-blowing special effects, over 300
                                                lavish costumes and a
                                                fabulous cast bring the magic of Disney’s Aladdin to life on stage.
                                                Featuring all the songs from the classic Academy award winning film,
                                                including “Friend like Me”,
                                                “A Whole New World” and “Arabian Nights”, prepare to experience the
                                                unmissable theatrical magic
                                                that is Aladdin.</p>

                                        </div>
                                    </div>
                                </div>
                                <div className="card line">
                                    <div className="card-header pos-1" id="headingThree">
                                        <button className="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="false"
                                                aria-controls="collapseThree">
                                            <span>Admission Rules</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>

                                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree"
                                         data-parent="#detail-accordion">
                                        <div className="card-body">
                                            <p className="p1"> Playing final performances with limited VIP seats remaining, book
                                                now before tickets sell out!
                                                From the producer of The Lion King, the beloved story of Aladdin comes to
                                                life in this spectacular
                                                new musical. Breathtaking sets, mind-blowing special effects, over 300
                                                lavish costumes and a
                                                fabulous cast bring the magic of Disney’s Aladdin to life on stage.
                                                Featuring all the songs from the classic Academy award winning film,
                                                including “Friend like Me”,
                                                “A Whole New World” and “Arabian Nights”, prepare to experience the
                                                unmissable theatrical magic
                                                that is Aladdin.</p>

                                        </div>
                                    </div>
                                </div>
                                <div className="card line shadow-sm">
                                    <div className="card-header pos-1" id="headingFour">
                                        <button className="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseFour" aria-expanded="true"
                                                aria-controls="collapseFour">
                                            <span>Gallery</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>

                                    <div id="collapseFour" className="collapse show" aria-labelledby="headingFour"
                                         data-parent="#detail-accordion">
                                        <div className="card-body text-center">
                                            <iframe src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-5 col-12 mob-px-0">
                            <div className="d-none d-sm-none d-md-block">
                                <div className="col-md-12 mb-4">
                                    <button className="btn btn-primary btn-outline">
                                        <img className="mr-2" src="assets/images/seat-map.png" /> Seat Map
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-12 col-12 mob-px-0 accordion">

                                <div className="card line">
                                    <div className="card-header pos-1" id="headingOne-1">
                                        <button className="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseOne-1" aria-expanded="false" aria-controls="collapseOne-1">
                                            <span>Price Details</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>
                                    <div id="collapseOne-1" className="collapse" aria-labelledby="headingOne-1"
                                         data-parent="#accordionExample">
                                        <div className="card-body">
                                            <p className="p1"> Playing final performances with limited VIP seats remaining, book now
                                                before tickets sell out!
                                                From the producer of The Lion King, the beloved story of Aladdin comes to life
                                                in this spectacular
                                                new musical. Breathtaking sets, mind-blowing special effects, over 300 lavish
                                                costumes and a
                                                fabulous cast bring the magic of Disney’s Aladdin to life on stage.
                                                Featuring all the songs from the classic Academy award winning film, including
                                                “Friend like Me”,
                                                “A Whole New World” and “Arabian Nights”, prepare to experience the unmissable
                                                theatrical magic
                                                that is Aladdin.</p>
                                        </div>
                                    </div>
                                </div>

                                <div className="card line">
                                    <div className="card-header pos-1" id="headingTwo-1">
                                        <button className="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseTwo-1" aria-expanded="treu" aria-controls="collapseOne-1">
                                            <span>Promotions</span>
                                            <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                        </button>
                                    </div>
                                    <div id="collapseTwo-1" className="collapse show" aria-labelledby="headingTwo-1"
                                         data-parent="#accordionExample">
                                        <div className="card-body internal-accordion">
                                            <div className="card-header" id="headingTwo-1">
                                                <button className="btn btn-link" type="button" data-toggle="collapse"
                                                        data-target="#20-percent" aria-expanded="true" aria-controls="collapseOne-1">
                                                    <span>20% Discount for Cool Tix & Silver Tix</span>
                                                    <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                                </button>
                                            </div>
                                            <div id="20-percent" className="collapse show" aria-labelledby="headingTwo-1"
                                                 data-parent="#accordionExample">
                                                <div className="card-body">
                                                    <ul className="dot-ul">
                                                        <li>Applicable to all shows (excluding Friday & Saturday evning shows)</li>
                                                        <li>Applicable to D Reserve & E Reserve only</li>
                                                        <li>Please kindly note that stringent cheeck will be conducted at the door for Cool Tix & Sliver Tix tickets, do bring along a valid identity for admission</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="card-header" id="headingTwo-1">
                                                <button className="btn btn-link" type="button" data-toggle="collapse"
                                                        data-target="#10-percent" aria-expanded="false" aria-controls="collapseOne-1">
                                                    <span>10% Discount for Phantastic 4 Bundle</span>
                                                    <i><img src="assets/images/chevron-right-dark.svg" alt="icon"/></i>
                                                </button>
                                            </div>
                                            <div id="10-percent" className="collapse" aria-labelledby="headingTwo-1"
                                                 data-parent="#accordionExample">
                                                <div className="card-body">
                                                    <ul className="dot-ul">
                                                        <li>Applicable to all shows (excluding Friday & Saturday evning shows)</li>
                                                        <li>Applicable to D Reserve & E Reserve only</li>
                                                        <li>Please kindly note that stringent check will be conducted at the door for Cool Tix & Sliver Tix tickets, do bring along a valid identity for admission</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


            </section>
        )
    }
}







