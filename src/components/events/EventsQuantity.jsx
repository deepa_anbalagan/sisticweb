import React, {Component, Fragment} from 'react'

export class Breadcrumbs extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
        <Fragment>
            <section class="show-info">
                <div class="container">
                    <div class="row">
                        <div class="media col-md-9 col-12">
                            <div class="media-img mr-4">
                                <img class="img-fluid" src={process.env.PUBLIC_URL + "/images/poster.jpg"} alt="Generic placeholder image"/>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 ft">Aladdin - The Hit Broadway Musical</h5>
                                <p class="ft-1">Featuring all the songs from the classic Academy award winning film, including
                                    "Friend like Me", "A Whole New World" and "Arabian Nights",

                                    prepare to experience the unmissable theatrical magic that is Aladdin.</p>

                            </div>
                        </div>
                        <div class="col-md-3 d-none d-sm-none d-md-block">
                            <div class="countdown-container">
                                <div class="circle">
                                    <svg viewBox="0 0 220 220" xmlns="http://www.w3.org/2000/svg">
                                        <g transform="translate(110,110)">
                                        <circle r="100" class="e-c-base"/>
                                        <g transform="rotate(-90)">
                                        <circle r="100" class="e-c-progress"/>
                                        <g id="e-pointer">
                                        <circle cx="98" cy="0" r="10" class="e-c-pointer"/>
                                        </g>
                                        </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="controlls">
                                    <div class="display-remain-time">15:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="show-info float-wrap d-none d-sm-none d-md-block">
                <div class="container">
                    <div class="row">
                        <div class="media col-md-9">
                            <div class="media-body">
                                <h5 class="mt-0 ft">Aladdin - The Hit Broadway Musical</h5>
                                <p class="ft-1">Featuring all the songs from the classic Academy award winning film, including
                                    "Friend like Me", "A Whole New World" and "Arabian Nights",

                                    prepare to experience the unmissable theatrical magic that is Aladdin.</p>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="countdown-container">
                                <div class="circle">
                                    <svg viewBox="0 0 220 220" xmlns="http://www.w3.org/2000/svg">
                                        <g transform="translate(110,110)">
                                        <circle r="100" class="e-c-base"/>
                                        <g transform="rotate(-90)">
                                        <circle r="100" class="e-c-progress"/>
                                        <g id="e-pointer">
                                        <circle cx="98" cy="0" r="10" class="e-c-pointer"/>
                                        </g>
                                        </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="controlls">
                                    <div class="display-remain-time">15:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="container-fluid ticket-quantity">
                <div class="row">
                    <div class="container">
                        <h4 class="title-bdr">How many people are going?</h4>
                        <div class="col-md-12 ticket-list px-0">

                            <div class="tic-list-item col-md-12 px-0">
                                <div class="bar-img active d-block d-sm-block d-md-none">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="bar-img d-none d-sm-none d-md-block">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="ticket-bar">
                                    <span class="tic-left">Adult</span>
                                    <div class="tic-right button-container">
                                        <button class="cart-qty-minus" type="button" value="-">
                                            <img src={process.env.PUBLIC_URL + "/images/minus.svg"} alt="icon"/>
                                        </button>
                                        <input type="text" name="qty" class="qty form-in" maxlength="12" value="2" class="input-text qty" />
                                        <button class="cart-qty-plus" type="button" value="+">
                                            <img src={process.env.PUBLIC_URL + "/images/plus.svg"} alt="icon"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tic-list-item col-md-12 px-0">
                                <div class="bar-img d-block d-sm-block d-md-none">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="bar-img d-none d-sm-none d-md-block">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="ticket-bar">
                                    <span class="tic-left">Child
                                        <i class="tootltip-icon" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Child (2-12 years)">
                                            <img src={process.env.PUBLIC_URL + "/images/info-tooltip.svg"} alt="icon"/>
                                        </i>
                                    </span>
                                    <div class="tic-right button-container">
                                        <button class="cart-qty-minus" type="button" value="-">
                                            <img src={process.env.PUBLIC_URL + "/images/minus.svg"} alt="icon"/>
                                        </button>
                                        <input type="text" name="qty" class="qty form-in" maxlength="12" value="0" class="input-text qty" />
                                        <button class="cart-qty-plus" type="button" value="+">
                                            <img src={process.env.PUBLIC_URL + "/images/plus.svg"} alt="icon"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tic-list-item col-md-12 px-0">
                                <div class="bar-img d-block d-sm-block d-md-none">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img-active.svg" alt="ticket"/>
                                </div>
                                <div class="bar-img d-none d-sm-none d-md-block">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="ticket-bar">
                                    <span class="tic-left">NSF</span>
                                    <div class="tic-right button-container">
                                        <button class="cart-qty-minus" type="button" value="-">
                                            <img src={process.env.PUBLIC_URL + "/images/minus.svg"} alt="icon"/>
                                        </button>
                                        <input type="text" name="qty" class="qty form-in" maxlength="12" value="0" class="input-text qty" />
                                        <button class="cart-qty-plus" type="button" value="+">
                                            <img src={process.env.PUBLIC_URL + "/images/plus.svg"} alt="icon"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tic-list-item col-md-12 px-0">
                                <div class="bar-img d-block d-sm-block d-md-none">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-mob-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="bar-img d-none d-sm-none d-md-block">
                                    <img class="img" src={process.env.PUBLIC_URL + "/images/tic-item-img.svg"} alt="ticket"/>
                                    <img class="img-blue" src={process.env.PUBLIC_URL + "/images/tic-item-img-active.svg"} alt="ticket"/>
                                </div>
                                <div class="ticket-bar">
                                    <span class="tic-left">Senior Citizen
                                        <i class="tootltip-icon" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Senior (65 years & above)">
                                            <img src={process.env.PUBLIC_URL + "/images/info-tooltip.svg"} alt="icon"/>
                                        </i>
                                    </span>
                                    <div class="tic-right button-container">
                                        <button class="cart-qty-minus" type="button" value="-">
                                            <img src={process.env.PUBLIC_URL + "/images/minus.svg"} alt="icon"/>
                                        </button>
                                        <input type="text" name="qty" class="qty form-in" maxlength="12" value="0" class="input-text qty" />
                                        <button class="cart-qty-plus" type="button" value="+">
                                            <img src={process.env.PUBLIC_URL + "/images/plus.svg"} alt="icon"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 px-0 text-right mt-5">
                            <button class="btn btn-primary mt-2 mob-fix-btm in-active">
                                Select Date & Time
                                <img class="ml-3" src={process.env.PUBLIC_URL + "/images/btn-right-arrow.svg"} alt="icon"/>
                            </button>
                        </div>

                    </div>
                </div>
            </section>
        </Fragment>
        )
    }
}
