import React, {Component} from 'react'

export class SimilarPicks extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <section className="sec-4 smiliar-pick">
                <div className="container-fluid pad-0">
                    <div className="row mt-5 mb-4">
                        <div className="col-md-8 col-8"><h3 className="head">Similar Picks</h3></div>
                        <div className="col-md-4 col-4 text-right d-block d-sm-block d-md-none">
                            <a href="javascript:;" className="see-all">
                                See all
                                <img src={process.env.PUBLIC_URL + "/assets/images/chevron-right-blue.svg"} alt="icon"/>
                            </a>
                        </div>
                    </div>
                    <div className="owl-controls d-none d-sm-none d-md-block">
                        <div className="custom-nav owl-nav"></div>
                    </div>

                    <div className="row blog">

                        <div className="col-md-12 col-12 mob-px-0">
                            <div className="blog-carousel owl-theme col-md-12 col-12 px-0">

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-1.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Fri, 3 May 2019</span>
                                        <label className="badge badge-green">Concert</label>
                                        <h4 className="title">SSO Red Balloon Sries: Rhythums, Rites</h4>
                                        <span>Esplanade Concert Hall</span>
                                    </div>
                                </a>

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-2.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Sun, 8 Dec 2019</span>
                                        <label className="badge badge-purple">Dance</label>
                                        <h4 className="title">Singapore Dance Theatre - Season Pass 2019</h4>
                                        <span>Various Venues</span>
                                    </div>
                                </a>

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-3.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Sun, 11 Aug 2019</span>
                                        <label className="badge badge-blue">Musical</label>
                                        <h4 className="title">Aladdin - The Hit Broadway Musical</h4>
                                        <span>Sands Theatre at Marina Bay Sands</span>
                                    </div>
                                </a>

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-1.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Fri, 3 May 2019</span>
                                        <label className="badge badge-green">Concert</label>
                                        <h4 className="title">SSO Red Balloon Sries: Rhythums, Rites</h4>
                                        <span>Esplanade Concert Hall</span>
                                    </div>
                                </a>

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-2.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Sun, 8 Dec 2019</span>
                                        <label className="badge badge-purple">Dance</label>
                                        <h4 className="title">Singapore Dance Theatre - Season Pass 2019</h4>
                                        <span>Various Venues</span>
                                    </div>
                                </a>

                                <a href="javascript:;" className="item">
                                    <div className="blog-img">
                                        <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/blog-thumb-3.png"}
                                                alt="img"/>
                                    </div>
                                    <div className="blog-cont">
                                        <span className="date">Sun, 11 Aug 2019</span>
                                        <label className="badge badge-blue">Musical</label>
                                        <h4 className="title">Aladdin - The Hit Broadway Musical</h4>
                                        <span>Sands Theatre at Marina Bay Sands</span>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}




