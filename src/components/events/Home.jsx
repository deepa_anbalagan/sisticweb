import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {Footer} from '../common/Footer'
import {Header} from '../common/Header'
import * as actionCreators from "../../actions/actionCreator"
import * as API from "../../service/events/eventsInfo"
import {Articles} from "./Articles";
import {Description} from "./Description";


class Home extends Component {

    constructor(props) {
        super(props);

    }

    async componentDidMount() {

        API.getInfo().then(response =>{
            this.props.fetchData(response);
        });
    }

    render() {
        return (

            <Fragment>
                <Header/>

                <main>

                    <section className="sec-1">
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-lg-8 col-md-12 px-0">
                                    <div className="show-slider">
                                        <div id="big" className="owl-carousel owl-theme">
                                            <div className="item">
                                                <img src="assets/images/banner-img-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/banner-img-2.jpg"/>
                                            </div>
                                        </div>
                                        <div id="thumbs" className="owl-carousel owl-theme">
                                            <div className="item">
                                                <img src="assets/images/thumbnail-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-1.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-2.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-3.jpg"/>
                                            </div>
                                            <div className="item">
                                                <img src="assets/images/thumbnail-2.jpg"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 col-md-12 float-right mtt-5 res-mtt0 mob-px-0 bannRgt">
                                    <div className="col-md-12 ml-0 pl-0 pr-0 mt-2 float-left">
                                        <div className="col-md-12 mt-2 clearfix vert_middle">
                                            <p className="d-none d-sm-none d-md-block breadCrumb">Home / Musicals</p>
                                            <div
                                                className="d-block d-sm-block d-md-none label-sec float-left vert_middle">
                                                <span className="badge badge-primary">Musical</span>
                                                <span className="badge badge-outline">12+ Years</span>
                                            </div>
                                            <a href="#" className="float-right share-hdr"><img width="100%" height="50%"
                                                                                               src="assets/images/share-icon.png"/></a>
                                            <a href="#"
                                               className="d-block d-sm-block d-md-none float-right share-hdr"><img
                                                width="100%" height="50%" src="assets/images/info-icon.png"/></a>
                                        </div>
                                        <div
                                            className="d-none d-sm-none d-md-block col-md-12 mt-4 label-sec vert_middle">
                                            <span className="badge badge-primary">Musical</span>
                                            <span className="badge badge-outline">Dance</span>
                                        </div>

                                        <div className="col-md-12">
                                            <h4 className="mt-4 font-weight-bold show-title">Aladdin - The Hit Broadway
                                                Musical</h4>
                                        </div>

                                        <div className="col-md-12 mt-3 f-rubik clearfix">
                                            <div>
                                                <img className="mt-0 mr-2 float-left vert_middle"
                                                     src="assets/images/calendar-icon.png"/>
                                                <p className="text-muted float-left mb-0 dateCnt">Fri, 19 Apr - Sun, 19
                                                    May 2019<a href="javascript:;"
                                                               className="d-block d-sm-block d-md-none">View all Dates &
                                                        Time</a></p>

                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 f-rubik clearfix d-block d-sm-block d-md-none">
                                            <div>
                                                <img className="mt-0 mr-2 float-left vert_middle"
                                                     src="assets/images/map-icon-grey.png"/>
                                                <p className="text-muted float-left mb-0 viewAllVenues">Sands Theatre
                                                    Marina Bay Sands<a href="javascript:;" className="d-block">View all
                                                        Venues</a></p>
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 f-rubik d-block d-sm-block d-md-none">
                                            <div>
                                                <img className="mt-0 mr-2 float-left vert_middle"
                                                     src="assets/images/seat-grey-icon.png"/>
                                                <p className="text-muted seatMapOut"><a href="javascript:;"
                                                                                        className="d-block">Seat Map</a>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 f-rubik d-block d-sm-block d-md-none">
                                            <div>
                                                <img className="mt-0 mr-2 float-left vert_middle"
                                                     src="assets/images/dollar-grey-icon.png"/>
                                                <p className="text-muted prc_cnt">S$45 - S$380</p>
                                            </div>
                                        </div>
                                        <div className="d-none d-sm-none d-md-block">
                                            <div className="col-md-12 mt-3 f-rubik">
                                                <div>
                                                    <img className="mt-0  mr-2 ml-0 float-left"
                                                         src="assets/images/map-icon.png"/>
                                                    <p className="ml-3 locationTxt">Sands Theatre Marina Bay Sands</p>
                                                </div>
                                            </div>

                                            <div className="col-md-12 mt-4">
                                                <div>
                                                    <h6 className="font-weight-bold text-muted">Price</h6>
                                                    <h4 className="font-weight-bold price">S$45 - S$380</h4>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md-12 mt-4 mb-4">
                                            <a href="#">
                                                <div className="col-md-12 btn-primary my-2">
                                                    Buy Tickets
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <Description/>

                    <section className="button-sec container-fluid mob-px-0">
                        <hr/>
                        <div className="container-fluid btm-badge mb-5 pl-3">
                            <span className="ml-3 badge badge-outline">Entertainment</span>
                            <span className="badge badge-outline">Family</span>
                            <span className="badge badge-outline">Fun</span>
                        </div>

                    </section>
                    <Articles/>
                    <section className="sec-4 smiliar-pick">
                        <div className="container-fluid pad-0">
                            <div className="row mt-5 mb-4">
                                <div className="col-md-8 col-8"><h3 className="head">Similar Picks</h3></div>
                                <div className="col-md-4 col-4 text-right d-block d-sm-block d-md-none">
                                    <a href="javascript:;" className="see-all">
                                        See all
                                        <img src="assets/images/chevron-right-blue.svg" alt="icon"/>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-controls d-none d-sm-none d-md-block">
                                <div className="custom-nav owl-nav"></div>
                            </div>

                            <div className="row blog">

                                <div className="col-md-12 col-12 mob-px-0">
                                    <div className="blog-carousel owl-theme col-md-12 col-12 px-0">

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-1.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Fri, 3 May 2019</span>
                                                <label className="badge badge-green">Concert</label>
                                                <h4 className="title">SSO Red Balloon Sries: Rhythums, Rites</h4>
                                                <span>Esplanade Concert Hall</span>
                                            </div>
                                        </a>

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-2.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Sun, 8 Dec 2019</span>
                                                <label className="badge badge-purple">Dance</label>
                                                <h4 className="title">Singapore Dance Theatre - Season Pass 2019</h4>
                                                <span>Various Venues</span>
                                            </div>
                                        </a>

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-3.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Sun, 11 Aug 2019</span>
                                                <label className="badge badge-blue">Musical</label>
                                                <h4 className="title">Aladdin - The Hit Broadway Musical</h4>
                                                <span>Sands Theatre at Marina Bay Sands</span>
                                            </div>
                                        </a>

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-1.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Fri, 3 May 2019</span>
                                                <label className="badge badge-green">Concert</label>
                                                <h4 className="title">SSO Red Balloon Sries: Rhythums, Rites</h4>
                                                <span>Esplanade Concert Hall</span>
                                            </div>
                                        </a>

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-2.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Sun, 8 Dec 2019</span>
                                                <label className="badge badge-purple">Dance</label>
                                                <h4 className="title">Singapore Dance Theatre - Season Pass 2019</h4>
                                                <span>Various Venues</span>
                                            </div>
                                        </a>

                                        <a href="javascript:;" className="item">
                                            <div className="blog-img">
                                                <img className="img-fluid" src="assets/images/blog-thumb-3.png"
                                                     alt="img"/>
                                            </div>
                                            <div className="blog-cont">
                                                <span className="date">Sun, 11 Aug 2019</span>
                                                <label className="badge badge-blue">Musical</label>
                                                <h4 className="title">Aladdin - The Hit Broadway Musical</h4>
                                                <span>Sands Theatre at Marina Bay Sands</span>
                                            </div>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </main>

                <Footer/>
            </Fragment>
        );
    }
}


const mapStateToProps = (state) => {
    return state
};


export default connect(mapStateToProps, (dispatch) => {
        return {
            fetchData: (dataObject) => {
                dispatch(actionCreators.fetchShowInfo(dataObject));
            }
        }
    }
)(Home);

