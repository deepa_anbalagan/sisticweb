$(document).ready(function(){
    $(".search_icon").click(function(){
        $(this).parent("form").find("input").toggle();
    });

    /*button submit on footer */
    $(".button-arrow").click(function(){
        $(this).parent("form").submit();
    });

    /*seats get seat btn */
    $('.get-seat-btn').click(function(){
        if($(window).width() > 768){
            if($(this).text() == "Choose my own seats"){
                $(this).text('Get the best seats available');
            }else{
                $(this).text('Choose my own seats');
            }
        }
       
        if($(window).width() < 768){
            if($(this).text() == "Best seats"){
                $(this).text('Cancel');
                $(this).addClass('btn-bg');

            }else{
                $(this).text('Best seats');
                $(this).removeClass('btn-bg');
            }
        }
    });
    /*filter level change image */
    $(".filter-level img").mouseenter(function(){
        var newimg = $(this).data("newsrc");
        $(this).attr("src", newimg);
    });
    $(".filter-level img").mouseleave(function() {
        var oldimg = $(this).data("oldsrc");
        $(this).attr("src", oldimg);
    });

    
    $('#filter-layer-slider').owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        dots: false,
        margin: 10,
        responsive:{
            0:{
                items:3
            },
            600:{
                items:4
            },
            1000:{
                items:0
            }
        }
    })
    /*filter levels in mobile */
    $(".filter-layers").click(function(){
        $(this).parents(".filter-mobile").find(".filter-level").toggle();
    });

    $(".filter-main").click(function(){
        $(".filter-level-inner .filter-main").css({"opacity":"0.4"});
        $(this).css({"opacity":"1"});
    });
    $(".filter-main.all-filter").click(function(){
        $(".filter-level-inner .filter-main").css({"opacity":"1"});
    });

});

// ligh-slider

// optional
$('#blogCarousel').carousel({
    interval: 5000
});